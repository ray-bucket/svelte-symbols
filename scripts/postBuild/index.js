import * as fsp from 'fs/promises';

async function main() {
  const packageJson = await fsp.readFile('package.json', 'utf8');
  const json = JSON.parse(packageJson);

  delete json.devDependencies;
  delete json.scripts;

  fsp.writeFile('dist/package.json', JSON.stringify(json, null, 2) + '\n');

  return null;
}

main();
