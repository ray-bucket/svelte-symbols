import createSvelteIcon from './createSvelteIcon.js';
import getSvgNames from './getSvgNames.js';
import printProgress from './printProgress.js';
import clearIcons from './clearIcons.js';

async function main() {
  await clearIcons();
  const svgs = await getSvgNames();
  const components = [];

  for (let i = 0; i < svgs.length; i++) {
    const name = svgs[i];
    components.push(await createSvelteIcon(name));
    printProgress(i + 1, svgs.length);
  }
  console.log('\n\ndone');
}

main();
