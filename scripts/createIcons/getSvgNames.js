import * as fsa from 'fs/promises';
import * as path from 'path';
import { INPUT_PATH } from './constants.js';

export default async function getSvgNames() {
  const files = await fsa.readdir(INPUT_PATH);
  
  const names = files
    .map((name) => path.parse(name).name)
    .filter((name) => !name.endsWith('-fill'));

  // return names.slice(500, 600);
  return names;
}
