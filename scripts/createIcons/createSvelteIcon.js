import * as fsa from 'fs/promises';
import { INPUT_PATH, OUTPUT_PATH } from './constants.js';
import createComponentName from './createComponentName.js';
import { parse } from 'node-html-parser';

/**
 * @param { string } rawAttributes 
 * @returns { Record<string, string> }
 */
function attributesToObject(rawAttributes) {
  const regex = /(\w+)="([^"]+)"/g;
  return [...rawAttributes.matchAll(regex)].reduce((obj, match) => {
    const [, attribute, value] = match;
    obj[attribute] = value;
    return obj;
  }, {});
}

/**
 * 
 * @param {string} svgData 
 */
function getSvgContent(svgData) {
  const root = parse(svgData);
  const svg = root.childNodes[0];
  const paths = root.childNodes[0].childNodes;
  root.childNodes = paths;

  const content = root.toString();
  const attributes = attributesToObject(svg.rawAttrs);

  return { attributes, content };
}

/**
 * @param {string} svgName 
 */
export default async function createSvelteIcon(svgName) {
  const filledSvgName = `${svgName}-fill`;

  const svgData = await fsa.readFile(`${INPUT_PATH}/${svgName}.svg`, 'utf-8');
  const filledSvgData = await fsa.readFile(`${INPUT_PATH}/${filledSvgName}.svg`, 'utf-8');

  const svg = getSvgContent(svgData);
  const filledSvg = getSvgContent(filledSvgData);

  const outputFileName = createComponentName(svgName);

  await fsa.writeFile(`${OUTPUT_PATH}/${outputFileName}.svelte`, `
<script lang="ts">
  export let filled: boolean = false;
</script>

<!-- svelte-ignore a11y-mouse-events-have-key-events -->
<svg
  viewBox="${svg.attributes.viewBox}"
  width="24"
  height="24"
  fill="currentColor"
  {...$$restProps}
  on:click
  on:mouseover
  on:mouseenter
  on:mouseleave
  on:keydown
>
  {#if filled}
    ${filledSvg.content}
  {:else}
    ${svg.content}
  {/if}
</svg>
  `.trim() + '\n');

  return outputFileName;
}
