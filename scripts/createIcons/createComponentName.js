/**
 * @param { String } str
 */
export default function createComponentName(str) {
  let result = str.toLowerCase().replace(/([-_][a-z])/g, (group) =>
    group
      .toUpperCase()
      .replace(/[-_]/, '')
  );

  // capitalize
  result = result.charAt(0).toUpperCase() + result.slice(1);

  // if starts with a number, add "Symbol" prefix
  if (/^[0-9]/.test(result)) {
    result = `Symbol${result}`;
  }

  return `${result}Icon`;
}
