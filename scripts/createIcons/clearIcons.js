import * as fsa from 'fs/promises';
import { OUTPUT_PATH } from './constants.js';

export default async function clearIcons() {
  const files = (await fsa.readdir(OUTPUT_PATH)).filter(file => file.endsWith('Icon.svelte'));

  await Promise.all(
    files.map((file) => fsa.rm(`${OUTPUT_PATH}/${file}`))
  );
}
