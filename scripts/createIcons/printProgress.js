import * as readline from 'readline';

/**
 * @param {number} current 
 * @param {number} total 
 */
export default function printProgress(current, total) {
  const size = 20;
  const progress = Math.floor((current / total) * size);
  const filled = progress;
  const empty = size - progress;
  const filledPercentage = Math.floor((filled / size) * 100);

  const progressBar = `[${"=".repeat(filled)}${" ".repeat(empty)}]`;

  readline.clearLine(process.stdout, 0);
  readline.cursorTo(process.stdout, 0);
  process.stdout.write(`Processing: ${progressBar} ${filledPercentage}%`);
}
