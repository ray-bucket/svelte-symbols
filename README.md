# Icons Bucketh
All svg of Material Symbols as Svelte components

## Installation
You can install this package with your favorite package manager

```sh
# pnpm
pnpm i @bucketh/ui

# yarn
yarn add @bucketh/ui

# npm
npm install @bucketh/ui
```

## Usage
```svelte
<script lang="ts">
  import ChatIcon from '@bucketh/icons/ChatIcon.svelte';
  import CloudIcon from '@bucketh/icons/CloudIcon.svelte';
</script>

<div>
  <ChatIcon />
  <ChatIcon filled />
</div>

<div style="color: red">
  <CloudIcon />
  <CloudIcon filled />
</div>
```

## Search icons
All material symbols are [here](https://fonts.google.com/icons) but in pascal case and "Icon" suffixed (for example `SettingsIcon` or `InfoIcon`).

For named icons starting with number, added "Symbol" prefix, for example `Symbol4kIcon` and `Symbol123Icon`.

The icons ending in "-fill" are considered as variant and can be used with `filled` prop, for example `home` is `<HomeIcon />` and `home-fill` is `<HomeIcon filled />`

(the svg were taken from [@material-symbols/svg-400](https://www.npmjs.com/package/@material-symbols/svg-400))
